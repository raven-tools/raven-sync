import asyncio
from fastapi import FastAPI, HTTPException
import httpx
from datetime import datetime, timedelta
from pydantic import BaseModel, EmailStr
import hashlib
from dotenv import load_dotenv
import os 
load_dotenv()

# Change these
INNOVATOR_SERVER_URL = os.getenv("INNOVATOR_SERVER_URL")
TOKEN_URL =  os.getenv("TOKEN_URL")

# ARAS credentials
ARAS_USERNAME = os.getenv("ARAS_USERNAME")
ARAS_PASSWORD =os.getenv("ARAS_PASSWORD")
ARAS_DATABASE =os.getenv("ARAS_DATABASE")

class Token(BaseModel):
    access_token: str
    expires_at: datetime

class NewTask(BaseModel):
    title:str 
    description: str 
    task_id: str 
    user_email: EmailStr

class MethodCall(BaseModel):
    method_name:str 
    method_parameters: dict


app = FastAPI()

async def get_new_token():
    # Make a request to obtain a new token
    payload = {
      "username": ARAS_USERNAME,
      "password": hashlib.md5(ARAS_PASSWORD.encode()).hexdigest(),
      "client_id": "IOMApp",
      "database": ARAS_DATABASE,
      "grant_type": "password",
      "scope": "Innovator"
    }
    headers = {"content-type": "application/x-www-form-urlencoded"}

    async with httpx.AsyncClient() as client:
        response = await client.post(TOKEN_URL, data=payload, headers=headers)
        if response.status_code != 200:
            print(response.content)
            raise HTTPException(status_code=response.status_code, detail="Failed to obtain auth token")
        
        # Parse the token and expiration time
        token_data = response.json()
        access_token = token_data["access_token"]
        expires_in = token_data["expires_in"]
        expires_at = datetime.now() + timedelta(seconds=expires_in)
        
        return Token(access_token=access_token, expires_at=expires_at)

current_token = Token(access_token="", expires_at=datetime.now())

async def refresh_token_if_needed():
    global current_token
    if datetime.now() >= current_token.expires_at:
        current_token = await get_new_token()

@app.post("/call_method")
async def call_method(prepared_call: MethodCall ):

    await refresh_token_if_needed()

    if not current_token.access_token:
            raise HTTPException(status_code=500, detail="Auth token is not available")

    headers  = {
        "Soapaction" : "ApplyAML",
        "Content-Type":"text/xml",
        "Authorization": f"Bearer {current_token.access_token}",
    }
    # print(prepared_call.method_parameters)
    query = f"""
    <AML>
        <Item type='Method' action='{prepared_call.method_name}'>
                 <todo>Remove the HELLL below</todo>
                {"".join([ "<{0}><![CDATA[{1}]]></{0}>".format(k,v)    for k, v in prepared_call.method_parameters.items()])}
        </Item>
    </AML>
    """ 
    print(query)

    async with httpx.AsyncClient() as client:
        response = await client.post(url=INNOVATOR_SERVER_URL, data=query, headers=headers )
        print(response) 
        print(response.content) 
    return {"message": "Hello World"}

@app.get("/")
async def testing_root():
    return "Yes the server is up!!"

async def main():
    await refresh_token_if_needed()
    if current_token.access_token is None:
        raise "Could not generate a access token!"

if __name__ == "__main__":
    asyncio.run(main())
