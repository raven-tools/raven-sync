# Raven Auth

One stop shop for all your ARAS API auth needs.
Blazingly Fast. Rigorously secure. Unfailingly reliable.


## Installation

```bash
python -m pip install -r requirements.txt
```


## Usage

Change `TOKEN_URL` and `INNOVATOR_SERVER_URL` before running the server.  

Also change the credentials for aras login.


```bash
fastapi dev raven-agent.py
```
